// Create a location/storage where the friends list will be stored.

let friendsList = [];
displayMessage = document.getElementById('responseMessage');
displayFriends = document.getElementById('listOfFriends');

function AddNewFriend() {
	let friend = document.getElementById('friendName').value;

	if (friend !== '') {
		friendsList.push(friend);
		displayMessage.innerHTML = `<h5 class=" ml-5 text-wrap text-success mt-2">Successfully added ${friend}</h5>`
	}
	else {
		displayMessage.innerHTML = '<h5 class=" ml-5 text-wrap text-danger mt-2">Input is Invalid</h5>'
	}
}

function removeLastFriend() {
	displayMessage.innerHTML = `<h5 class=" ml-5 text-wrap text-warning mt-2">Removed ${friendsList.slice(-1)}</h5>`
	friendsList.pop();
}
console.log(friendsList);



